package fr.esgi.training.spark.streaming

import fr.esgi.training.spark.utils.SparkUtils
import org.apache.spark.sql.types.StructType
import org.apache.spark.sql.{DataFrame, Dataset, SparkSession}
import org.apache.spark.sql.functions._


/******************************************
  * Question Initial
  *****************************************/

/**
  * Question 1 :
  *   Il contient 4 colonnes :
  *   - sexe de la personnes [Nombre]
  *   - prénom donné à l'enfant [String]
  *   - année de naissance [Nombre]
  *   - nombre de personne ayant ce prénom [Nombre]
  *
  * Question 2 :
  *
  *
  */

/******************************************
  * Découvrir DataStream
  *****************************************/

object StreamingNames {

  val spark = SparkSession
    .builder()
    .appName("Spark")
    .master("local[4]")
    .getOrCreate()

  import spark.implicits._

  def main(args: Array[String]): Unit = {

    val df_init = spark.readStream
      .format("socket")
      .option("host", "localhost")
      .option("port", 9999)
      .load()

    //Créez une nouvelle colonne où les données sont splittée
//    val df = df_init.withColumn("splitted", SparkUtils.split(col("value")))

    // Transformez votre DataStream pour obtenir une structure utilisable
    val df = df_init.filter(!col("value").contains("sexe"))
      .withColumn("sexe", SparkUtils.sexe(col("value")))
//      .withColumn("preusuel", SparkUtils.preusuel(col("value")))
//      .withColumn("annais", SparkUtils.annais(col("value")))
//      .withColumn("nombre", SparkUtils.nombre(col("value")))
      .drop("value")

    // Mode Complete
    // Sexe 1 : 79166
    val df_res = df.select("sexe").groupBy("sexe").count()
//      .agg(col("preusuel"))


//    val df_res = df_init

    // Mode Append :
    // Sur la console, on reçoit 563 batchs, avec un premier avec 0 données
    // Le job se termine apres 3 min.
    val append = df_res.writeStream
      .outputMode("append")

    // Mode Update :
    // Sur la console, on reçoit 555 batchs, avec un premier avec 0 données
    // job se termine après 3 min
    val update = df_res.writeStream
      .outputMode("update")

    // Mode Complete
    // Le job plante et demande
    // "Complete output mode not supported when there are no streaming aggregations on streaming DataFrames/Datasets"
    // Ajout de "groupBy("value").count()"
    // Sur la console, on reçoit 2 batchs, avec un premier avec 0 données
    // Dans ce mode, il attends la transmission de toute les données
    val complete = df_res.writeStream
      .outputMode("complete")

//    append
//    update
    complete
      .format("console")
      .start()
      .awaitTermination(180000)
  }



}
