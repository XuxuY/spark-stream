package fr.esgi.training.spark.utils

import org.apache.spark.SparkConf
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions.udf

object SparkUtils {

  val sparkConf = new SparkConf()

  def initExercice(exercice : String) = {
    sparkConf.set("spark.app.name", exercice)
  }

  def spark() = {
    sparkConf.set("spark.master", "local[4]")

    val session = SparkSession.builder()
      .config(sparkConf)
      .getOrCreate()

    session
  }

  val split= udf((id: String) => {
    id.split(";")
  })

  val sexe = udf((line: String) =>  {
    line.split(";")(0).trim()
  })

  val preusuel = udf((line: String) =>  {
    line.split(";")(1).trim()
  })
  val annais = udf((line: String) =>  {
    line.split(";")(2).trim()
  })
  val nombre = udf((line: String) =>  {
    line.split(";")(3).trim()
  })
}